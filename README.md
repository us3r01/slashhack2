# Helping Hand

## 1.   Description

It  aims to bridge the distance between NGOs and lessfortunate people all around the streets. Users can create a public record of the Beggars, Child under labours and all sorts of Vagrants found on the streets. NGOs can view this records on the Website or on Application itself and contact the people and try to help them. 

This Android application is built on .NET and Xamarin Framework , and hence, can also be ported across Major platforms – iOS and Windows.

>You need at least Android API level 18 (Jelly Bean) to run this application. And, we have used Mock GPS location for testing purposes.

## 2.   Objectives
  - User Friendly Interface
    -    A more engaging and easy-to-use graphical user interface for all types of users, ranging from developers to non-technical sound users.
    -    Easy Login/Signup and pages to create and view New Records.

 - Creating New Records
    -    Application provides clear and detailed description on how to create new records by Users.
    -    Users can click the photos of the person and fill in the required information and submit it to the database.

- NGO Record-viewing
    -    NGO can view the records and go in contact with the selected personnel.
    -    NGO can provide the appropriate response and solution to the personnel and close the required Record with appropriate documents uploaded.

- Publically available Data
    -    All the data uploaded by NGO/Users is publically available on a Website for people to see. Thus, maintaining transparency and discrepancy.
    -    People can choose to question the data uploaded by NGO and can reopen the Record if not satisfied with the Data.

## 3.   Features
This prototype application contains some of the Industry-standard features such as 

- Material Design.

- Faster Bootup.

- Easier Login and Signup.

- Ability to create New Records.

- Upload Records to Database in real-time.

- NGOs can change the Application status.

- Near Real-time data update.

- List of all records available in One-go.

- Website support to publically view Data.

- Uses APIs to get the content in real time.

# 4.    Running The Project
Let  me remind you again that the minimum Android OS that you need to run this project is Jelly Bean (API Level 18). So, make sure you're satisfying the minimum requirements first. Otherwise, your handset won't be able to parse the apk file.

- ### Permissions Required :
    This application requires you to provide few permissions to it, in order to work properly. Here's the list of permissions that the application needs :
    - Internet Access
    - View WiFi Connections
    - Storage (Read/Write Perms For Cache)
    - Read Google Service Configuration
    - Camera (For Taking Pictures)

- #### Instructions For Direct APK Installation :

    If you want to run this application on your android phone, please move over to the "[`Downloads`](https://bitbucket.org/us3r01/slashhack2/downloads/)" section and download the latest stable APK build for your android phone. You do not need any external libraries or application.

- #### Instructions For Developers/Testers :

     If you're a developer or any user who wishes to test this application and run this android project, it's recommended to install Visual Studio with Xamarin Support and Android SDKs on your system. Remember that Android SDKs should be in your local path for you to be able to compile the project properly. You can find the source code in the "[SOURCE](https://bitbucket.org/us3r01/slashhack2/src/5a89eb650e6afd17426bbca5b682cdbf552462ec/src/?at=master)" directory.

    If you do not happen to have Visual Studio, it is recommended to get it because it'll download all the required packages on its own, if they're not present. You can use Visual Studio's Free Community Edition. It'll work, as we've developed this application on it.
But, if for some reason, you don't want to or can't install Visual Studio, you will need to have .NET, Xamarin, Android SDK and required Packages in your system's local path for you to be able to compile and execute this application project.

You can check the Demonstration Video On YouTube :

[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/VideoCode/0.jpg)](https://youtu.be/VideoCode)